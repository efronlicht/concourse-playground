package integration_test

import (
	"os"
	"testing"
	"time"
)

func Test_Example_Slow_Integration(t *testing.T) {
	if testing.Short() {
		t.Skip("this test pretends to be slow")
	}
	time.Sleep(100 * time.Millisecond)
}

func Test_Example_Needs_Outside_Service(t *testing.T) {
	if testing.Short() {
		t.Skip("this test pretends to need a database")
	}
	if os.Getenv("FAKE_POSTGRES_CREDENTIALS") == "" {
		t.Fatal("missing environment variable FAKE_POSTGRES_CREDENTIALS")
	}
}
