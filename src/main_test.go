package main

import "testing"

func Test_Example_UnitTest(t *testing.T) {
	for b := 'A'; b <= 'E'; b++ {
		t.Run(string([]rune{b}), func(t *testing.T) {}) // also no-ops
	}
}
